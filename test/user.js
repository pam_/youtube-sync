/* eslint-env mocha */
/* eslint-disable no-unused-vars */

process.env.TEST_ENV = true

let User = require('../models/User')

let chai = require('chai')
let chaiHttp = require('chai-http')
let server = require('../server')
let should = chai.should()

chai.use(chaiHttp)

describe('Users', () => {
  beforeEach((done) => {
    User.remove({}, (err) => {
      if (err) throw err
      done()
    })
  })

  describe('GET /users', () => {
    it('should get an error if there are no users to GET', (done) => {
      chai.request(server)
        .get('/users')
        .end((err, res) => {
          if (err) throw err
          res.should.have.status(400)
          res.body.should.be.a('object')
          res.body.should.have.property('error')
          res.body.error.should.have.property('id').eql(2)
          done()
        })
    })

    // it('should GET all the users', (done) => {
    //   chai.request(server)
    //     .get('/users')
    //     .end((err, res) => {
    //       if (err) throw err
    //       res.should.have.status(200)
    //       res.body.should.be.a('array')
    //       res.body.length.should.be.eql(0)
    //       done()
    //     })
    // })
  })
  describe('/POST /users', () => {
    it('should not POST with no username', (done) => {
      let user = {
        password: 'password'
      }
      chai.request(server)
        .post('/users')
        .send(user)
        .end((err, res) => {
          if (err) throw err
          res.should.have.status(400)
          res.body.should.be.a('object')
          res.body.should.have.property('error')
          res.body.error.should.have.property('id').eql(3)
          done()
        })
    })
    it('should not POST with no password', (done) => {
      let user = {
        username: 'username'
      }
      chai.request(server)
        .post('/users')
        .send(user)
        .end((err, res) => {
          if (err) throw err
          res.should.have.status(400)
          res.body.should.be.a('object')
          res.body.should.have.property('error')
          res.body.error.should.have.property('id').eql(3)
          done()
        })
    })
    it('should POST a user', (done) => {
      chai.request(server)
        .post('/users')
        .type('form')
        .send({
          'username': 'firstUser',
          'password': 'password'
        })
        .end((err, res) => {
          if (err) throw err
          res.should.have.status(201)
          res.body.should.be.a('object')
          res.body.should.have.property('_id')
          res.body.should.have.property('username').eql('firstUser')
          done()
        })
    })
    it('should trim the username field', (done) => {
      chai.request(server)
        .post('/users')
        .type('form')
        .send({
          'username': '  spaced  ',
          'password': 'password'
        })
        .end((err, res) => {
          if (err) throw err
          res.should.have.status(201)
          res.body.should.have.property('username').eql('spaced')
          done()
        })
    })
    it('should not POST a used username', (done) => {
      // Creates first user named used
      chai.request(server)
        .post('/users')
        .type('form')
        .send({
          'username': 'used',
          'password': 'password'
        })
        .end((err, res) => {
          if (err) throw err
          // Creates second user named used once the first one is created
          chai.request(server)
            .post('/users')
            .type('form')
            .send({
              'username': 'used',
              'password': 'safePass'
            })
            .end((err, res) => {
              if (err) throw err
              res.should.have.status(400)
              res.body.should.be.a('object')
              res.body.should.have.property('error')
              res.body.error.should.have.property('id').eql(4)
              done()
            })
        })
    })
    it('should not POST a password that is too small', (done) => {
      chai.request(server)
        .post('/users')
        .type('form')
        .send({
          'username': 'simpleuser',
          'password': 'small'
        })
        .end((err, res) => {
          if (err) throw err
          res.should.have.status(400)
          res.body.should.be.a('object')
          res.body.should.have.property('error')
          res.body.error.should.have.property('id').eql(1)
          done()
        })
    })
  })
})
