/* eslint-env mocha */
/* eslint-disable no-unused-vars */

process.env.TEST_ENV = true

let chai = require('chai')
let chaiHttp = require('chai-http')
let server = require('../server')
let should = chai.should()

chai.use(chaiHttp)

describe('Login', () => {
  describe('GET /login', () => {
    it('should return the login form', (done) => {
      chai.request(server)
        .get('/login')
        .end((err, res) => {
          if (err) throw err
          res.should.have.status(200)
          done()
        })
    })
  })
})
