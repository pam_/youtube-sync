let mongoose = require('mongoose')
let uniqueValidator = require('mongoose-unique-validator')

let Schema = mongoose.Schema

mongoose.Promise = global.Promise

let UserSchema = new Schema({
  username: {type: String, required: true, unique: true, trim: true},
  password: {type: String, required: true, trim: true}
})

UserSchema.index({_id: 1, username: 1})
UserSchema.plugin(uniqueValidator)

module.exports = mongoose.model('User', UserSchema)
