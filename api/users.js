let bcrypt = require('bcrypt')
let User = require('../models/User')
let utils = require('../utils')

function getUser (username, callback) {
  let query = User.findOne({ 'username': username })
  query.select('_id username')
  query.exec((err, user) => {
    if (err) throw err
    if (!user) return callback(utils.errorMessage(2), null)
    callback(null, user)
  })
}

function getUsers (callback) {
  let query = User.find({})
  query.select('_id username')
  query.exec((err, users) => {
    if (err) throw err
    if (users.length < 1) return callback(utils.errorMessage(2), null)
    callback(null, users)
  })
}

function postUser (userSent, callback) {
  _validateUsernameAndPassword(userSent.username, userSent.password, (err) => {
    if (err) return callback(err, null)
    let username = userSent.username.trim()
    let password = userSent.password.trim()
    _encryptPassword(password, (err, hash) => {
      if (err) return callback(err, null)
      let user = new User({
        username: username,
        password: hash
      })

      let promise = user.save()
      promise.then(() => {
        callback(null, { _id: user._id, username: user.username, created: 'yes' })
      }).catch((err) => {
        callback(err, null)
      })
    })
  })
}

function _encryptPassword (password, callback) {
  bcrypt.hash(password, 12, (err, hash) => {
    if (err) return callback(err, null)
    callback(null, hash)
  })
}

function _validateUsernameAndPassword (username, password, callback) {
  if (username === undefined || password === undefined) return callback(utils.errorMessage(3)) // Missing fields
  if (username.trim().length === 0 || password.trim().length === 0) return callback(utils.errorMessage(3)) // Fields empty
  if (username.trim().length < 3) return callback(utils.errorMessage(5)) // Username too short
  if (password.trim().length < 8) return callback(utils.errorMessage(1)) // Password too short
  _isUnique(username.trim(), (err) => {
    if (err) return callback(err)
    callback(null)
  })
}

function _isUnique (username, callback) {
  getUser(username, (err, user) => {
    // getUser returns an error when no users are found
    if (err && err.error.id === 2) return callback(null)
    if (err) return callback(err)
    callback(utils.errorMessage(4))
  })
}

module.exports = { getUser, getUsers, postUser }
