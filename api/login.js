let User = require('../models/User')

let bcrypt = require('bcrypt')

function login (username, password, callback) {
  _getUserPassword(username, (err, hash) => {
    if (err) return callback(err, null)
    _comparePasswordAndHash(password, hash, (err, match) => {
      if (err) return callback(err, null)
      callback(null, match)
    })
  })
}

function _getUserPassword (username, callback) {
  let query = User.findOne({ 'username': username })
  query.select('password')
  query.exec((err, pass) => {
    if (err) return callback(err, null)
    callback(null, pass.password)
  })
}

function _comparePasswordAndHash (password, hash, callback) {
  bcrypt.compare(password, hash, (err, result) => {
    if (err) return callback(err, null)
    callback(null, result)
  })
}

module.exports = { login }
