const express = require('express')
const app = express()

let http = require('http')
let bodyParser = require('body-parser')
let mongoose = require('mongoose')
let session = require('express-session')

let usersRoute = require('./routes/users')
let loginRoute = require('./routes/login')
let dashboardRoute = require('./routes/dashboard')
let logoutRoute = require('./routes/logout')
let utils = require('./utils')

let PORT = process.env.PORT || 4000

if (process.env.TEST_ENV) {
  mongoose.connect(process.env.MONGO_URI + '/test', {autoIndex: false})
} else {
  mongoose.connect(process.env.MONGO_URI + '/webapp', {autoIndex: false})
}

let db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'))

app.use(bodyParser.urlencoded({extended: true}))
app.use(session({
  secret: 'CHANGESECRETAs@P48590',
  resave: false,
  saveUninitialized: true
}))

app.set('view engine', 'ejs')

app.get('/', (req, res) => {
  res.render('pages/index', {
    title: 'Home',
    pageId: 'home',
    logged: utils.isLoggedIn(req)
  })
})

app.get('/about', (req, res) => {
  res.render('pages/about', {
    title: 'About',
    pageId: 'about',
    logged: utils.isLoggedIn(req)
  })
})

app.get('/register', (req, res) => {
  res.render('pages/register', {
    title: 'Register',
    pageId: 'register',
    logged: utils.isLoggedIn(req)
  })
})

app.route('/login')
  .get(loginRoute.getLogin)
  .post(loginRoute.postLogin)

app.route('/users')
  .get(usersRoute.getUsers)
  .post(usersRoute.postUser)

app.route('/users/:id')
  .get(usersRoute.getUser)

app.route('/dashboard')
  .get(dashboardRoute.getDashboard)

app.route('/logout')
  .get(logoutRoute.getLogout)

let httpServer = http.createServer(app)

httpServer.listen(PORT)

module.exports = httpServer
