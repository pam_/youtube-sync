let utils = require('../utils')

function getLogout (req, res) {
  if (!utils.isLoggedIn(req)) return res.status(401).redirect('/')

  req.session.username = null
  res.status(200).redirect('/')
}

module.exports = { getLogout }
