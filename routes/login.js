let loginAPI = require('../api/login')
let utils = require('../utils')

function getLogin (req, res) {
  res.render('pages/login', {
    title: 'Login',
    pageId: 'login',
    logged: utils.isLoggedIn(req)
  })
}

function postLogin (req, res) {
  if (!req.body.username || !req.body.password) return res.status(400).send(utils.errorMessage(3))

  loginAPI.login(req.body.username, req.body.password, (err, match) => {
    if (err) return res.status(400).send(err)
    if (match) {
      req.session.username = req.body.username
      res.redirect('/dashboard')
    } else {
      res.status(401).redirect('/login')
    }
  })
}

module.exports = { getLogin, postLogin }
