let utils = require('../utils')

function getDashboard (req, res) {
  res.render('pages/dashboard', {
    title: 'Dashboard',
    pageId: 'dashboard',
    logged: utils.isLoggedIn(req)
  })
}

module.exports = { getDashboard }
