let usersAPI = require('../api/users')

function getUsers (req, res) {
  usersAPI.getUsers((err, users) => {
    if (err) {
      res.status(400).send(err)
    } else {
      res.json(users)
    }
  })
}

function postUser (req, res) {
  let user = {
    username: req.body.username,
    password: req.body.password
  }

  usersAPI.postUser(user, (err, user) => {
    if (err) {
      res.status(400).send(err)
    } else {
      res.status(201).json(user)
    }
  })
}

function getUser (req, res) {
  usersAPI.getUser(req.params.id, (err, user) => {
    if (err) {
      res.status(400).send(err)
    } else {
      res.json(user)
    }
  })
}

module.exports = { getUsers, postUser, getUser }
