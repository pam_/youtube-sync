let errorMessages = require('./configs/errormessages.json')

function isLoggedIn (req) {
  if (req.session.username) return true
  return false
}

function errorMessage (id) {
  return ({
    error: {
      id: id,
      message: errorMessages[id]
    }
  })
}

module.exports = {isLoggedIn, errorMessage}
